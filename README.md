## Обычный запуск 

Если на компьютере python c версией ниже чем 3.10.0 или не знаете, какой установлен
- [Установить pyenv](https://github.com/pyenv/pyenv)

Посмотреть текущие версии python на компьютере
```sh
pyenv versions
```

Поменять версию 
```sh
pyenv local 3.10.0
```

Проверить что поменяли
```sh
pyenv local
```

Чтобы поменять версию python в poetry (не обязательно)
```sh
poetry env use python3.10
```

Активировать виртуальное окружение
```sh
poetry shell
```

Установить зависимости
```sh
poetry install
```

Создать файл .env в корне проекта и заполнить по аналогии с env.example

Провести миграции
```sh
python manage.py migrate
```

Создать фейковых пользователей с постами
```sh
python manage.py fake_users
```

Запустить сервер
```sh
python manage.py runserver
```

В другом терминале запустить постоянную отправку отправку писем
```sh
python manage.py send_emails
```

При необходимости перед предыдущей компандой активировать виртуальное окружение
```sh
poetry shell
```
