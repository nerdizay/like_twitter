FROM python:3.10

ENV PYTHONFAULTHANDLER=1 \
    PYTHONUNBUFFERED=1 \
    PYTHONHASHSEED=random \
    PIP_NO_CACHE_DIR=off \
    PIP_DISABLE_PIP_VERSION_CHECK=on \
    PIP_DEFAULT_TIMEOUT=100 \
    POETRY_VERSION=1.6.1 \
    PIP_VERSION=23.2.1

RUN apt-get update \
	&& apt-get install -y --no-install-recommends \
		postgresql-client \
	&& rm -rf /var/lib/apt/lists/*

RUN pip install --upgrade "pip==$PIP_VERSION" && pip install "poetry==$POETRY_VERSION"

WORKDIR /app

# RUN pip install poetry
# RUN curl -sSL https://install.python-poetry.org | python3 -
# RUN poetry shell
# COPY poetry.lock pyproject.toml README.md ./
COPY poetry.lock pyproject.toml README.md /app/
RUN poetry config virtualenvs.create false
RUN poetry check
RUN poetry install --no-interaction --no-ansi



# COPY requirements.txt ./
# RUN pip install -r requirements.txt
COPY . .
# COPY . /app/

EXPOSE 8000
