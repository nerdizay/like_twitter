import django
django.setup()
from django.core import mail
from django.conf import settings
from logging import Logger
import time
from multiprocessing import Process
from django.contrib.auth.models import User
from like_twitter_app.models import Post
from datetime import date
from django.template import Context, Template
from django.core.mail import send_mail


template = Template("""
<h1 style="display: block; width: 100%; color: red; font-weight: bold;">Здравствуй, {{user}}!</h1>
<h2 style="display: block; width: 100%;">Вот последние новости за день</h2>
<br>
<ol>
    {%for post in posts %}
    <li>
        <blockquote>
            <h3> {{ post.title }} </h3>
            <p> {{ post.post }} </p>
            <footer><em>{{ post.user }}</em></footer>
        </blockquote>
    </li>
    {% endfor %}
</ol>
""")


logger = Logger("logger")

def send_message_to_email(user, title: str = "", message: str = "", fake: bool = False):
    if fake:
        print(f"Типа отправляем письмо на почту пользователю {user}")
        time.sleep(5)
        return
    if user.email is None:
        logger.warning(f"У пользователя {user} по какой-то причине не обнаружили email.")
        return
    with mail.get_connection() as connection:
        send_mail(
            title,
            message,
            settings.EMAIL_HOST_USER,
            [user.email],
            fail_silently=False,
            connection=connection,
            html_message=message,
        )

def take_users_newslines_from_db_by_portion(portion: int = 10):
    for i in range(0, 10_000_000):
        from_ = i*portion+1
        to_ = i*portion+portion
        users = User.objects.filter(id__lt=to_, id__gt=from_).all()
        if len(users) == 0:
            break
        else:
            res = []
            for user in users:
                res.append({
                    "user": user,
                    "newsline": Post.objects.filter(user__in=user.profile.blog_favorites.all())[:5]
                })
            yield res

def send_message_to_email_worker():
    """
    Если вдруг случится так, что за целые сутки рассылка не успеет разослать всем письма, то она продолжит на другой день,
    а остатки дня будет отдыхать => будут получать письма не каждый день, а через день.
    Но если мы запустим поздним вечером и рассылка успевает за сутки => через день всё нормализуется.
    """
    day_sending_done = None
    while True:
        if day_sending_done is not None and day_sending_done == date.today():
            time.sleep(10)
        else:
            gen_users_info = take_users_newslines_from_db_by_portion(10)
            while True:
                try:
                    
                    users_info = next(gen_users_info)
                except StopIteration:
                    break
                
                else:
                    for info in users_info:
                        user = info["user"]
                        posts = info["newsline"]
                        send_message_to_email(
                            user,
                            title="Новостная рассылка",
                            message=template.render(Context({
                                "user": user,
                                "posts": posts,
                            })),
                            fake=True
                        )
                    
            day_sending_done = date.today()
    
def run_underground(worker):
    p = Process(target=worker)
    p.start()

