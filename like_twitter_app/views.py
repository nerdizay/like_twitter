from django.shortcuts import render
from rest_framework import generics, mixins, status, parsers, views
from rest_framework.pagination import PageNumberPagination
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.exceptions import NotFound, NotAuthenticated
from rest_framework.decorators import api_view, renderer_classes
from rest_framework.renderers import JSONRenderer, BrowsableAPIRenderer
from rest_framework.response import Response
from rest_framework.decorators import permission_classes, authentication_classes
from .models import Post, UserProfile, User
from .serializers import PostSerializer, BlogSerializer, ReadPostSerializer, NewsLineSerializer


class PostListCreateView(generics.ListCreateAPIView):
    class PostPagination(PageNumberPagination):
        page_size = 10
        
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = PostSerializer
    pagination_class = PostPagination

    def get_queryset(self):
        user = self.request.user
        return Post.objects.filter(user=user).all()
    
class BlogListView(generics.ListAPIView):
    class BlogPagination(PageNumberPagination):
        page_size = 10
        
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = BlogSerializer
    pagination_class = BlogPagination
    
    def get_queryset(self):
        return User.objects.order_by('-id').all()
    
    
class BlogRetrieveView(generics.RetrieveAPIView):

    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAuthenticated]
    lookup_field = 'id'
    serializer_class = BlogSerializer
    
    def get_queryset(self):
        user_id = self.kwargs['id']
        return User.objects.filter(id=user_id).all()
    
        
@api_view(('POST','DELETE'))
@renderer_classes((JSONRenderer, BrowsableAPIRenderer))
@permission_classes((IsAuthenticated,))
@authentication_classes((BasicAuthentication, SessionAuthentication))
def subscription(request, id=None):
    blog = User.objects.filter(id=id)
    if not blog.exists():
        raise NotFound
    
    blog = blog.get()
    user = request.user
    
    if request.method == "POST":
        user.profile.subscribe(blog)
    elif request.method == "DELETE":
        user.profile.unsubscribe(blog)
    
    return Response({}, status=status.HTTP_200_OK)
 

class NewsLineListView(generics.ListAPIView):
    class NewsLinePagination(PageNumberPagination):
        page_size = 10
        
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = NewsLineSerializer
    pagination_class = NewsLinePagination

    def get_queryset(self):
        user = self.request.user
        return Post.objects.filter(user__in=user.profile.blog_favorites.all())
    
class NewsLineRetrieveView(generics.RetrieveAPIView):
    """
    Нет проверки на то, чтобы пост от фаворита, на которого пользователь подписан.
    Почему? Ну, потому что ничего страшного если увидит пост от человека, на которого не подписан.
    + Фронт вряд ли перепутает id, а брать он будет их из newsline скорее всего.
    """
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAuthenticated]
    lookup_field = 'id'
    serializer_class = PostSerializer
    
    def get_queryset(self):
        post_id = self.kwargs['id']
        return Post.objects.filter(id=post_id).all()
    

@api_view(('POST','DELETE'))
@renderer_classes((JSONRenderer, BrowsableAPIRenderer))
@permission_classes((IsAuthenticated,))
@authentication_classes((BasicAuthentication, SessionAuthentication))
def reading(request, id=None):
    post = Post.objects.filter(id=id)
    if not post.exists():
        raise NotFound
    
    post = post.get()
    user = request.user
    
    if request.method == "POST":
        read_post = user.profile.read(post)
    elif request.method == "DELETE":
        read_post = user.profile.unread(post)
    serializer = ReadPostSerializer(read_post)
    return Response(serializer.data, status=status.HTTP_200_OK)




