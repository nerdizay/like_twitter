from rest_framework import serializers
from rest_framework.exceptions import NotAuthenticated
from .models import Post, User, ReadPost
from django.utils import timezone

class PostSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Post
        fields = '__all__'
        read_only_fields = ('user',)
        
    def to_representation(self, instance):
        data = super().to_representation(instance)
        data['user'] = instance.user.username
        return data
        
    def create(self, validated_data):
        return super().create(validated_data)
    
    def validate(self, data):
        return data
    
class NewsLineSerializer(serializers.ModelSerializer):
    
    read = serializers.SerializerMethodField()

    def get_read(self, serializer):
        user = self.context['request'].user
        read_post = ReadPost.objects.filter(user=user.id, post=serializer.id).first()
        if read_post is None:
            return False
        return read_post.is_read
    
    class Meta:
        model = Post
        fields = '__all__'
        read_only_fields = ('user',)
        
    def to_representation(self, instance):
        data = super().to_representation(instance)
        data['user'] = instance.user.username
        return data
        
    def create(self, validated_data):
        return super().create(validated_data)
    
    
    def validate(self, data):
        return data
    
    
class BlogSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = User
        fields = ('id', 'username')
        
    def to_representation(self, instance):
        data = super().to_representation(instance)
        return data
        
    def create(self, validated_data):
        return super().create(validated_data)
    
    def validate(self, data):
        return data
    
class ReadPostSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = ReadPost
        fields = '__all__'
        
    def to_representation(self, instance):
        data = super().to_representation(instance)
        return data
        
    def create(self, validated_data):
        return super().create(validated_data)
    
    def validate(self, data):
        return data
    