from django.db import models
from django.contrib.auth.models import User
from psqlextra.types import PostgresPartitioningMethod
from psqlextra.models import PostgresPartitionedModel
from django.db.models.signals import post_save
from django.dispatch import receiver


class Post(PostgresPartitionedModel):
    class PartitioningMeta:
        method = PostgresPartitioningMethod.LIST
        key = ["user_id"]

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    title = models.CharField(max_length=140, null=False)
    post = models.CharField(max_length=140, null=False)
    created_at = models.DateTimeField(auto_now=True)
    
    class Meta:
        ordering = ('-id',)


class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name="profile")
    blog_favorites = models.ManyToManyField(User, related_name="subscribers")

    def subscribe(self, blog: User):
        """blog is the same as a user"""
        self.blog_favorites.add(blog)
        
    def unsubscribe(self, blog: User):
        """blog is the same as a user"""
        self.blog_favorites.remove(blog)
        
    def _read_or_unread(self, post: Post, is_read: bool):
        read_post = ReadPost.objects.filter(user=self.user.id, post=post.id)
        if not read_post.exists():
            read_post = ReadPost.objects.create(user=self.user.id, is_read=is_read, post=post.id)
        else:
            read_post = read_post.get()
            read_post.is_read = is_read
            read_post.save()
        return read_post
            
    def read(self, post: Post):
        return self._read_or_unread(post, True)
        
    def unread(self, post: Post):
        return self._read_or_unread(post, False)
        

@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        UserProfile.objects.create(user=instance)

@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()
    
    
class ReadPost(PostgresPartitionedModel):
    class PartitioningMeta:
        method = PostgresPartitioningMethod.LIST
        key = ["user"]

    user = models.IntegerField()
    is_read = models.BooleanField(default=False, null=False)
    post = models.IntegerField()
    
    class Meta:
        ordering = ('-id',)
        unique_together = ('user', 'post')
