from django.core.management.base import BaseCommand
from like_twitter_project.utils import run_underground, send_message_to_email_worker

class Command(BaseCommand):
    help = 'sending message to emails'

    def handle(self, *args, **kwargs):
        run_underground(send_message_to_email_worker)