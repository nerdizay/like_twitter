from django.core.management.base import BaseCommand
from mixer.backend.django import mixer
from django.contrib.auth.models import User
from like_twitter_app.models import Post
from django.contrib.auth.hashers import make_password, check_password

class Command(BaseCommand):
    help = 'create fake users'

    def handle(self, *args, **kwargs):
        
        for _ in range(0, 100):
            mixer.blend(Post)
        
        for user in User.objects.all():
            user.set_password(user.password)
            user.save()
        





