from django.apps import AppConfig


class LikeTwitterAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'like_twitter_app'
