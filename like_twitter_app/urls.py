from django.contrib import admin
from django.urls import path, include
from .views import (
    PostListCreateView,
    BlogListView,
    BlogRetrieveView,
    subscription,
    NewsLineListView,
    NewsLineRetrieveView,
    reading,
)

urlpatterns = [
    path('api/posts', PostListCreateView.as_view()),
    path('api/blogs', BlogListView.as_view()),
    path('api/blogs/<int:id>', BlogRetrieveView.as_view()),
    path('api/blogs/<int:id>/subscription', subscription),
    path('api/newsline', NewsLineListView.as_view()),
    path('api/newsline/<int:id>', NewsLineRetrieveView.as_view()),
    path('api/newsline/<int:id>/reading', reading),
]